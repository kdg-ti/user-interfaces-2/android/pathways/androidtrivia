package com.example.android.navigation

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.android.navigation.databinding.FragmentTitleBinding

// TODO (01) Create the new TitleFragment
// Select File->New->Fragment->Fragment (Blank)

// TODO (02) Clean up the new TitleFragment
// In our new TitleFragment

// TODO (03) Use DataBindingUtil.inflate to inflate and return the titleFragment in onCreateView
// In our new TitleFragment
// R.layout.fragment_title
// TOTO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {
    // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null

  private lateinit var binding: FragmentTitleBinding

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
      //  return inflater.inflate(R.layout.placeholder_layout, container, false)
      binding= FragmentTitleBinding.inflate(inflater,container,false)
      initialiseViews()
      Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
      return binding.root
    }

  override fun onAttach(context: Context) {
    super.onAttach(context)
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onStart() {
    super.onStart()
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onResume() {
    super.onResume()
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onPause() {
    super.onPause()
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onStop() {
    super.onStop()
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onDestroyView() {
    super.onDestroyView()
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  override fun onDetach() {
    super.onDetach()
    Log.i(javaClass.simpleName,"${object{}.javaClass.enclosingMethod?.name} called")
  }

  private fun initialiseViews() {
  //  binding.playButton.setOnClickListener { it.findNavController().navigate(R.id.action_titleFragment_to_gameFragment) }
    binding.playButton.setOnClickListener { it.findNavController()
      .navigate(TitleFragmentDirections.actionTitleFragmentToGameFragment()) }
    setHasOptionsMenu(true)
  }

   override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
     inflater.inflate(R.menu.options_menu,menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return NavigationUI.onNavDestinationSelected(item,requireView().findNavController())||super.onOptionsItemSelected(item)
  }

  companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TitleFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TitleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}